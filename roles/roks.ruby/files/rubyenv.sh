export GEM_HOME="/home/vagrant/.bundler"
export BUNDLE_PATH="$GEM_HOME"
export BUNDLE_BIN="$GEM_HOME/bin"
export BUNDLE_SILENCE_ROOT_WARNING=1
export PATH="$BUNDLE_BIN:$PATH"